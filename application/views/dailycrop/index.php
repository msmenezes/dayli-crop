
<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>App - Ajudar no plantio para otimizar e melhorar os alimentos| App :: Nasa</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="Ajudar no plantio saudável, sustentável, inteligente e gratuíto para todos, pensando no futuro e principalmente nas camadas menos favorecidas da sociedade." />
  <!-- css links -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/typo.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/hoverex-all.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/inner.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/caption-hover.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/circle-hover.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/slider.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
  <!-- /css links -->
  <!-- font files -->
  <link href='//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
  <!-- font files -->
  <!-- js files  -->
  <script src="js/js/SmoothScroll.min.js"></script>
  <script type="text/javascript" src="js/js/modernizr.custom.js"></script> 
  <!-- /js files -->
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="60">
  <!-- Fixed navbar -->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#mypage">DailyCrop - Welcome <?php echo $name;?></a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
       <ul class="nav navbar-nav navbar-right">
         <li class="active"><a href="#mypage">Home</a></li>
         <li><a href="#horta">Minha Horta</a></li>
         <li><a href="#irrigacao">Irrigação</a></li>
         <li><a href="#fitossanidade">Fitossanidade</a></li>
         <li><a href="#adubo">Adubo Orgânico</a></li>
         <li><a href="#compartilhe">Compartilhe sua produção </a></li>
         <!--li><a href="contact.html">Contatos</a></li-->
       </ul>
     </div><!--/.nav-collapse -->
   </div>
 </nav>
 <!-- /Fixed navbar -->  
 <div class="banner" id="mypage">
  <div id="content">
    <div id="slider">
     <img src="img/banner1.jpg" alt="Food" data-url="#1">
     <img src="img/banner2.jpg" alt="Everywhere" data-url="#2">
     <img src="img/banner3.jpg" alt="For" data-url="#3">
     <img src="img/banner4.jpg" alt="Everyone" data-url="#4">
   </div>
 </div> 
</div>
<div class="w3ls"></div>
<div class="content-section-a">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-sm-6">
        <div class="content1">
         <h3 class="section-heading slideanim">Agricultura inteligente!</h3>
         <hr class="section-heading-spacer slideanim">
         <div class="clearfix"></div>
         <p class="lead slideanim">O futuro está ao seu alcançe, seja no fortalecimento da agricultura familiar ou no incentivo desta no meio urbano.</p>
       </div>
     </div>
     <div class="col-lg-5 col-lg-offset-2 col-sm-6 slideanim">
      <ul class="grid cs-style">
       <li>
        <figure>
         <img src="img/farm1.jpg" alt="" class="img-responsive">

       </figure>
     </li>
   </ul> 
 </div>
</div>
</div>
</div>
<div class="w3ls"></div>
<div class="content-section-b">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-lg-offset-1 col-sm-push-6 col-sm-6">
        <div class="content2">
          <h3 class="section-heading slideanim">Nossa Proposta:</h3>
          <hr class="section-heading-spacer slideanim">
          <div class="clearfix"></div>
          <p class="lead slideanim">A nobre tarefa de produzir alimentos ao alcançe de suas mãos, estabelecendo o uso sustentável da Terra, diminuindo disperdícios, visando melhorar o manejo da água e produzindo oportunidades econômicas locais.</p>
        </div>
      </div>
      <div class="col-lg-5 col-sm-pull-6 col-sm-6 slideanim">
        <ul class="grid cs-style">
         <li>
          <figure>
           <img src="img/farm2.jpg" alt="" class="img-responsive">
         </figure>
       </li>
     </ul>
   </div>
 </div>
</div>
</div>
<div class="w3ls"></div>
<!-- Horta -->
<section class="our-blog" id="horta">
  <h3 class="text-center slideanim">Biblioteca de plantas</h3>
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-xs-12">
        <div class="news-w3ls">
          <div class="view view-seventh slideanim">
          <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
            <div class="caption">
              <div class="caption-content">
                <i class="fa fa-paper-plane"></i>
              </div>
            </div>
          <img src="img/blog1.jpg" alt="" class="img-responsive"/>
          <div class="mask">
            <h5>Data do plantio</h5>
            <p>Alface</p>
          </div>
          </a>
          </div>
        </div>
        <div class="blog-content slideanim">
          <p class="p1"></p>
          <h4>Ambiente aberto - 2 pés de alface.</p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="news-w3ls">
            <div class="view view-seventh slideanim">
              <img src="img/blog2.jpg" alt="" class="img-responsive"/>
              <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
            <div class="caption">
              <div class="caption-content">
                <i class="fa fa-paper-plane"></i>
              </div>
            </div>
          <img src="img/blog2.jpg" alt="" class="img-responsive"/>
          <div class="mask">
            <h5>Data do plantio</h5>
            <p>Alface</p>
          </div>
          </a>
            </div>
          </div>  
          <div class="blog-content slideanim">
            <p class="p1"></p>
            <h4>Ambiente aberto - 3 pés de tomate.</h4>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="news-w3ls">
            <div class="view view-seventh slideanim">
              <img src="img/blog3.jpg" alt="" class="img-responsive"/>
              <div class="mask">
                <h5>Data do plantio</h5>
                <p>Alho</p>
              </div>
            </div>
          </div>  
          <div class="blog-content slideanim">
            <p class="p1"></p>
            <a href="#"><h4>Ambiente coberto - 4 cabeças de alho.</h4></a>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="news-w3ls">
            <div class="view view-seventh slideanim">
              <img src="img/blog4.jpg" alt="" class="img-responsive"/>
              <div class="mask">
                <h5>Add</h5>
                <p>Adicionar</p>
              </div>
            </div>
          </div>  
          <div class="blog-content slideanim">
            <p class="p1"></p>
            <a href="#"><h4>Adicinar outro</h4></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /Blog -->
  <!-- /irrigacao -->
  <div class="content-section-a" id="irrigacao">
   <div class="container">
    <div class="row">
      <div class="col-lg-5 col-sm-6">
        <div class="content3">
         <h3 class="section-heading slideanim">Qual a quantidade de água ideal para cada tipo de planta?</h3>
         <hr class="section-heading-spacer slideanim">
         <div class="clearfix"></div>
         <p class="lead slideanim">Acompanhe diariamente a necessidade de aguar sua planta.</p>
       </div>
     </div>
     <div class="col-lg-5 col-lg-offset-2 col-sm-6 slideanim">
      <ul class="grid cs-style">
       <li>
        <figure>
         <img src="img/2019-10-20 (2).png" alt="" class="img-responsive">

       </figure>
     </li>
   </ul>
 </div>
</div>
</div>
</div>

<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade slideanim" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"></div>
      </div>
    </div>
    <div class="container">
     <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="modal-body">
          <h3>Alface</h3>
          <hr>
          <p style="font-size:3rem;">Pulgões - Pulverizar com uma solução à base de sabão de potássio a 1,5%.</p>
          <p style="font-size:3rem;">Fusariose - Elimine as plantas doentes.</p>
          <p style="font-size:3rem;">Míldio - Retire as folhas doentes. Desbaste o mais cedo possível. Cultive variedades resistentes.</p>
          <p><button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
            <button type="button" class="btn btn-default"> Visualizar Imagens</button></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body">
            <h3></h3>
            <hr>
            <img src="img/port2.jpg" class="img-responsive img-centered" alt="">
            <p></p>
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body">
            <h3></h3>
            <hr>
            <img src="img/port3.jpg" class="img-responsive img-centered" alt="">
            <p></p>
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                  <div class="lr">
                    <div class="rl"></div>
                  </div>
                </div>
                <div class="container">
                 <div class="row">
                  <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                      <?php if($cadastro[0]['produto'] == 'alface'){ ?>
                      <h3>Alface</h3>
                      <hr>
                      <p style="font-size:3rem;">Se você plantar hoje : <?php echo $cadastro[0]['data_plantio'] ? $cadastro[0]['data_plantio'] : "Não há plantação ainda."; ?></p>
                      <p style="font-size:3rem;">Dia aproximado da colheita: <?php echo date('Y/m/d', strtotime("+7 weeks", strtotime($cadastro[0]['data_plantio']))) ?>;
                      <p style="font-size:3rem;">O que deseja fazer : <?php echo $cadastro[0]['tipo']; ?> </p>
                      <p style="font-size:3rem;">Sua planta deve estar parecida com esta no dia de hoje:</p>
                      <figure>
                        <img src="img/blog1.jpg" alt="" class="img-responsive">
                      </figure>
                      <p><button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
                        <button type="button" class="btn btn-default"> Visualizar Imagens</button></p>
                        <?php }else{ ?>
                          <p style="font-size:3rem;">Você ainda não tem plantio.</p>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                  <div class="lr">
                    <div class="rl"></div>
                  </div>
                </div>
                <div class="container">
                 <div class="row">
                  <div class="col-lg-8 col-lg-offset-2">
                    <div class="modal-body">
                      <?php if($cadastro[0]['produto'] == 'tomate'){ ?>
                      <h3>Tomato</h3>
                      <hr>
                      <p style="font-size:3rem;">Se você plantar hoje : <?php echo $cadastro[0]['data_plantio'] ? $cadastro[0]['data_plantio'] : "Não há plantação ainda."; ?></p>
                      <p style="font-size:3rem;">Dia aproximado da colheita: <?php echo date('Y/m/d', strtotime("+12 weeks", strtotime($cadastro[0]['data_plantio']))) ?>;
                      <p style="font-size:3rem;">O que deseja fazer : <?php echo $cadastro[0]['tipo']; ?> </p>
                      <p style="font-size:3rem;">Sua planta deve estar parecida com esta no dia de hoje:</p>
                      <figure>
                        <img src="img/port2.jpg" alt="" class="img-responsive">
                      </figure>
                      <p><button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
                        <button type="button" class="btn btn-default"> Visualizar Imagens</button></p>
                        <?php }else{ ?>
                          <p style="font-size:3rem;">Você ainda não tem plantio.</p>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

<!-- /Irrigação -->
<!-- Portfolio Grid Section -->
<section id="fitossanidade">
  <h3 class="text-center slideanim">Como vamos lhe ajudar</h3>
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item slideanim">
        <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
         <div class="caption">
          <div class="caption-content">
           <i class="fa fa-paper-plane"></i>
         </div>
       </div>
       <img src="img/port1.jpg" class="img-responsive" alt="">
     </a>
   </div>
   <div class="col-md-4 col-sm-6 col-xs-12 portfolio-item slideanim">
    <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
     <div class="caption">
      <div class="caption-content">
       <i class="fa fa-paper-plane"></i>
     </div>
   </div>
   <img src="img/port2.jpg" class="img-responsive" alt="">
 </a>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 portfolio-item slideanim">
  <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
    <div class="caption">
      <div class="caption-content">
        <i class="fa fa-paper-plane"></i>
      </div>
    </div>
    <img src="img/port3.jpg" class="img-responsive" alt="">
  </a>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 portfolio-item slideanim">
  <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
    <div class="caption">
      <div class="caption-content">
        <i class="fa fa-paper-plane"></i>
      </div>
    </div>
    <img src="img/port4.jpg" class="img-responsive" alt="">
  </a>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 portfolio-item slideanim">
  <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
    <div class="caption">
      <div class="caption-content">
        <i class="fa fa-paper-plane"></i>
      </div>
    </div>
    <img src="img/port5.jpg" class="img-responsive" alt="">
  </a>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 portfolio-item slideanim">
  <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
    <div class="caption">
      <div class="caption-content">
        <i class="fa fa-paper-plane"></i>
      </div>
    </div>
    <img src="img/port6.jpg" class="img-responsive" alt="">
  </a>
</div>
</div>
</div>
</section>
<!-- /Portfolio Grid Section -->

<div class="portfolio-modal modal fade slideanim" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"></div>
      </div>
    </div>
    <div class="container">
     <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="modal-body">
          <h3></h3>
          <hr>
          <img src="img/port4.jpg" class="img-responsive img-centered" alt="">
          <p></p>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body">
            <h3></h3>
            <hr>
            <img src="img/port5.jpg" class="img-responsive img-centered" alt="">
            <p></p>
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="modal-body">
            <h3></h3>
            <hr>
            <img src="img/port6.jpg" class="img-responsive img-centered" alt="">
            <p></p>
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /Portfolio Modals -->

<!-- Adubo -->
<div class="content-section-a" id="adubo">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-sm-6">
        <div class="content3">
         <h3 class="section-heading slideanim">#ZeroDisperdício</h3>
         <hr class="section-heading-spacer slideanim">
         <div class="clearfix"></div>
         <p class="lead slideanim">Para fazer uma pilha de compostagem, separe quantidades iguais de materiais úmidos e secos (folhas secas, serragem), dispondo-os em camadas e cortando o excesso em pedaços pequenos. A última camada deve conter materiais secos. Umedeça tudo e deixe o recipiente em área com luz solar.</p>
       </div>
     </div>
     <div class="col-lg-5 col-lg-offset-2 col-sm-6 slideanim">
      <ul class="grid cs-style">
       <li>
        <figure>
         <img src="img/humus_adubo2.jpg" alt="" class="img-responsive">
       </figure>
     </li>
   </ul>
 </div>
</div>
</div>
</div>
<!-- /Contact -->
<!-- compartilhe -->
<section class="contact-us col-md-12" id="compartilhe">
  <h3 class="text-center slideanim">Compartilhe seu cultivo!</h3>
  <p class="text-center slideanim">"Sozinhos vamos mais rápidos, juntos chegamos mais longe!</p>
  <div class="container" id="compartilhe">
    <div class="row text-center">
      <div class="col-md-8 slideanim">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d886177.7583968674!2d-95.401291!3d29.817178000000002!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640b8b4488d8501%3A0xca0d02def365053b!2sHouston%2C%20TX%2C%20EUA!5e0!3m2!1spt-BR!2sbr!4v1571580319340!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
    </div>
  </div>
</section>
<section class="contact-form">
  <h3 class="text-center slideanim">Compartilhe sua produção</h3>
  <p class="text-center slideanim"></p>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 slideanim">
        <form action="<?= base_url('insertCadastro') ?>" name="sentMessage" id="contactForm" method="post">
          <div class="row">
            <div class="form-group col-lg-4">
              <label>Nome*</label>
              <input type="text" name="nome" value="<?php echo $name; ?>" class="form-control" placeholder="Nome" required>
            </div>
            <div class="form-group col-lg-4">
              <label>Email*</label>
              <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" placeholder="E-mail" required>
            </div>
            <div class="form-group col-lg-4">
              <label>CEP</label>
              <input type="text" name="cep" id="cep" class="form-control" placeholder="CEP" onchange="buscaEndereco(this.value,'')">
            </div>
            <div class="form-group col-lg-4">
              <label>Estado*</label>
              <input type="text" name="estado" id="estado_" class="form-control" placeholder="Estado" required>
            </div>
            <div class="form-group col-lg-4">
              <label>Cidade*</label>
              <input type="text" name="cidade" id="cidade_" class="form-control" placeholder="Cidade" required>
            </div>
            <div class="form-group col-lg-4">
              <label>Bairro*</label>
              <input type="text" name="bairro" id="bairro_" class="form-control" placeholder="Bairro" required>
            </div>
            <div class="form-group col-lg-4">
              <label>Rua*</label>
              <input type="text" name="rua" id="rua_" class="form-control" placeholder="Rua" required>
            </div>
            <div class="form-group col-lg-4">
              <label>Número*</label>
              <input type="text" name="numero" id="numero_" class="form-control" placeholder="Número" required>
            </div>
            <div class="form-group col-lg-4">
              <label>Celular*</label>
              <input type="tel" name="telefone" class="form-control" placeholder="Celular" required>
            </div>
            <div class="form-group col-lg-4">
              <label>Produto*</label>
              <select name="produto" class="form-control" required>
                <option value=""></option>
                <option  value="alface" style="color:black;">Alface</option>
                <option value="tomate" style="color:black;">Tomate</option>
                <option value="cebola" style="color:black;">Cebola</option>
              </select>
            </div>
            <div class="form-group col-lg-4">
              <label>Tipo*</label>
              <select name="tipo" class="form-control" required>
                <option value=""></option>
                <option value="doacao" style="color:black;">Doação</option>
                <option class="form-control" value="venda" style="color:black;">Venda</option>
              </select>
            </div>
            <div class="form-group col-lg-4">
              <label>Data do Plantio*</label>
              <input type="date" name="data_plantio" class="form-control" placeholder="Data do Plantio" required>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-lg-12">
              <label>Descrição do produto produzido.</label>
              <textarea name="mensagem" class="form-control" rows="6" placeholder="Descrição" required></textarea>
            </div>
            <div class="form-group col-lg-12">
              <button type="submit" class="btn btn-lg btn-outline">Enviar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>  
<!-- /Contact -->
<!-- Footer -->
<footer class="text-center">
  <div class="footer-above">
    <div class="container">
      <div class="row">
        <div class="footer-col col-md-4">
          <h4></h4>
          <!--p>3481 Melrose Place<br>Beverly Hills, Chicago 90210</p-->
        </div>
        <div class="footer-col col-md-4">
          <h4>@spacebeans</h4>
                    <!--ul class="list-inline">
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                          </li-->
                          <!--li-->
                          <a href="#" class="btn-social btn-outline">
                           <img src="img/social_7-32.png" class="img-responsive img-centered" alt="">
                         </a>
                        <!--/li>
                        <!--li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                          </li-->
                        </ul>
                      </div>
                      <div class="footer-col col-md-4">
                        <h4></h4>
                        <p></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="footer-below">
                  <div class="container">
                    <div class="row">
                     <div class="container">
                       <p>Copyright © 2019. All rights reserved | Template by <a href="http://w3layouts.com">W3layouts</a></p>
                     </div>
                   </div>
                 </div>
                 <a href="#myPage" title="To Top">
                   <span class="glyphicon glyphicon-chevron-up"></span>
                 </a>
               </div>
             </footer>
             <!-- js files -->
             <script src="js/js/jquery.min.js"></script>
             <script src="js/js/bootstrap.min.js"></script>
             <script src="js/js/classie.js"></script>
             <script src="js/js/TweenMax.min.js"></script>
             <script src="js/js/index.js"></script>
             <script src="js/js/index2.js"></script>
             <script>
              $(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

  // Store hash
  var hash = this.hash;

  // Using jQuery's animate() method to add smooth page scroll
  // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 900, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = hash;
  });
});
})
</script>
<script>
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
      if (pos < winTop + 600) {
        $(this).addClass("slide");
      }
    });
  });

  function buscaEndereco(cep, nome) {
    cep = cep.replace(/\D/g, '');
    //Verifica se campo cep possui valor informado.
    if (cep != '') {
        //Consulta o webservice viacep.com.br/
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
          if (!("erro" in dados)) {
                //Atualiza os campos com os valores da consulta.
                $("#rua" + "_" + nome).val(dados.logradouro);
                $("#cidade" + "_" + nome).val(dados.localidade);
                $("#estado" + "_" + nome).val(dados.uf);
                $("#bairro" + "_" + nome).val(dados.bairro);
                $("#numero" + "_" + nome).focus();
                //console.log(dados)
              } else {
                console.log('erro')
                $("#cep" + "_" + nome).val("");
                $("#rua" + "_" + nome).val("");
                $("#cidade" + "_" + nome).val("");
                $("#estado" + "_" + nome).val("");
                $("#bairro" + "_" + nome).val("");
                $("#cep" + "_" + nome).focus();
                alert("CEP não encontrado.");
              }
        }); //end if.
      } else {
        alert('Digite um número de cep válido.');
        $("#cep" + "_" + nome).val("");
        $("#rua" + "_" + nome).val("");
        $("#cidade" + "_" + nome).val("");
        $("#estado" + "_" + nome).val("");
        $("#bairro" + "_" + nome).val("");
        $("#cep" + "_" + nome).focus();
      }
    }

  </script>
  <script src="js/js/contact_me.js"></script>
  <!-- /js files -->
</body>
</html>